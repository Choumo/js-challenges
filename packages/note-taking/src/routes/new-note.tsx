import {createFileRoute, Link} from '@tanstack/react-router'
import Title from "../components/Headers/Title";
import React from "react";

export const Route = createFileRoute('/new-note')({
	component: () => CreateNote()
});

function CreateNote() {
	const [title, setTitle] = React.useState('');
	const [content, setContent] = React.useState('');

	const handleTitleChange = (event) => {
		setTitle(event.target.value);
	};

	const handleContentChange = (event) => {
		setContent(event.target.value);
	};

	const handleSubmit = (event) => {
		event.preventDefault();

		// (Optional) Process the note data here, e.g., store it in localStorage
		console.log('Note submitted:', { title, content });

		// Clear form fields (optional)
		setTitle('');
		setContent('');
	};

	return (
		<div className="create-note">
			<Title title={"Create a new note"}/>
			<hr/>
			<form onSubmit={handleSubmit}>
				<label htmlFor="title">
					Title:
					<input
						type="text"
						id="title"
						value={title}
						onChange={handleTitleChange}
						placeholder="Enter a note title..."
					/>
				</label>
				<label htmlFor="content">
					Content:
					<textarea
						id="content"
						value={content}
						onChange={handleContentChange}
						placeholder="Write your note here (Markdown supported)"
					/>
				</label>
				<button type="submit">Create Note</button>
			</form>
		</div>
	);
}
