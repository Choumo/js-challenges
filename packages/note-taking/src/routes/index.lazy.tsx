import {createLazyFileRoute, Link} from '@tanstack/react-router'

import Title from "../components/Headers/Title";

export const Route = createLazyFileRoute('/')({
	component: Index,
})

function Index() {
	return (
		<div className="p-2">
			<div className="title-container">
				<Title title={"Notes"}/>
				<Link to={"/"} className="link">Create a new note</Link>
			</div>

			<hr/>
		</div>
	)
}
