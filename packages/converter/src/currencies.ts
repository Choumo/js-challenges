class CurrencySingleton {
	private static instance: CurrencySingleton;

	static getInstance(): CurrencySingleton {
		if (!CurrencySingleton.instance) {
			CurrencySingleton.instance = new CurrencySingleton();
		}
		return CurrencySingleton.instance;
	}

	async getCurrencies() {
		const response: Response = await fetch("https://api.freecurrencyapi.com/v1/currencies?apikey=" + import.meta.env.VITE_CONVERTER_API_KEY, {
			method: "GET",
		});

		if(!response.ok) {
			alert("Une erreur est survenenue lors de l'appel à l'API");
		}

		return response.json();
	}

	async convertDevise(baseDevise = "EUR", targetDevise = "USD") {
			const response = await fetch("https://api.freecurrencyapi.com/v1/latest?base_currency=" + baseDevise + "&currencies=" + targetDevise + "&apikey=" + import.meta.env.VITE_CONVERTER_API_KEY, {
				method: "GET",
			});

			if(!response.ok) {
				alert("Une erreur est survenenue lors de l'appel à l'API");
			}

			return response.json();
	}
}

export default CurrencySingleton;
