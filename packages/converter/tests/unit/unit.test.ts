import type { CurrencyInterface } from "../../src/interfaces/currency";

import { describe, test, expect } from 'vitest';

import CurrencySingleton from "../../src/currencies.ts";

import { server } from "./mocks/node";
import { currencyConvertObject, currencyListObject } from "./mocks/objects/mock_objects";

server.listen();

describe('currency', () => {
	test("get currency list", async () => {
		const currencyModule = CurrencySingleton.getInstance();
		const currencyList: CurrencyInterface = await currencyModule.getCurrencies();
		expect(currencyList).toMatchObject(currencyListObject);
	});

	test("convert currency list", async () => {
		const currencyModule = CurrencySingleton.getInstance();
		const currencyList = await currencyModule.convertDevise("EUR", "USD");
		expect(currencyList).toMatchObject(currencyConvertObject);
	});
});
