import { setupServer } from 'msw/node';

import { handlers } from './handlers/handler.ts';

export const server = setupServer(...handlers)
