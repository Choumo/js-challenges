import { test, expect } from '@playwright/test';

// J'ai pas réussi à faire fonctionner playwright sur NixOS, mais tester c'est douter
test('Currency Converter Test', async ({ page }) => {
	await page.goto('http://localhost:4000');

	await page.waitForSelector('#baseDevise option:not(:disabled)');
	await page.waitForSelector('#targetDevise option:not(:disabled)');

	await page.selectOption('#baseDevise', { label: 'Euro - EUR' });

	await page.selectOption('#targetDevise', { label: 'US Dollar - USD' });

	await page.fill('#montant', '1');

	await page.click('button');

	await page.waitForSelector('#resultat');

	const resultValue = await page.inputValue('#resultat');
	expect(Number.parseFloat(resultValue)).toBeGreaterThan(0);
});
