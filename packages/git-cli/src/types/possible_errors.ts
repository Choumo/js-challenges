import type {CloneException} from "../exception/clone_exception.ts";

export type PossibleErrors = CloneException | Error;
