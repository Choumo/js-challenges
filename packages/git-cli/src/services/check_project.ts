import {execa} from "execa";

export async function checkIfIsGitProject() {
	const { stdout } = await execa` git rev-parse --is-inside-work-tree`;

	return stdout;
}
