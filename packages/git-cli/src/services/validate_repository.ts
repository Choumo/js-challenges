export class ValidateRepository {
	#repositoryUrl: string;

	private static instance: ValidateRepository;

	private constructor(url: string) {
		this.#repositoryUrl = url;
	}

	public static getInstance(url: string): ValidateRepository {
		if (!ValidateRepository.instance) {
			ValidateRepository.instance = new ValidateRepository(url);
		}
		return ValidateRepository.instance;
	}

	get isValidHTTPS (): boolean {
		if (this.#repositoryUrl.startsWith('https://')) {
			return true;
		} else {
			throw new Error("Url is not https");
		}
	}

	get isValidSSH(): boolean {
		if (this.#repositoryUrl.startsWith('git@')) {
			return true;
		} else {
			throw new Error("Url is not ssh");
		}
	}
}
