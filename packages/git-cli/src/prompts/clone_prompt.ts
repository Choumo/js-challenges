import type {Prompt} from "@poppinss/prompts";

import {execa} from "execa";

import {ValidateRepository} from "../services/validate_repository.js";

type CloneType = "https" | "ssh";

export async function ClonePrompt(prompt: Prompt) {
		const cloneType: CloneType = await prompt.choice("Select a type", [
			{
				name: "https",
				message: "HTTPS",
			},
			{
				name: "ssh",
				message: "SSH"
			},
		]);

		const repository: string = await prompt.ask("Paste your repository url : ", {
			validate(url : string): boolean {
				return url.length > 0;
			}
		});

		const clone: ValidateRepository = ValidateRepository.getInstance(repository);
		switch (cloneType) {
			case "https": {
				clone.isValidHTTPS;
				break;
			}
			case "ssh": {
				clone.isValidSSH;
				break;
			}
		}

		let specificRepositoryLocation = ".";
		const isSpecificLocation: boolean = await prompt.confirm('Do you want to clone the repository at a specific location ?');

		if (isSpecificLocation) {
			specificRepositoryLocation = await prompt.ask('Select the absolute path or the name of where you want to clone the repository (default : current directory): ');
		}

		try {
			await execa({
				stdout: ["pipe", "inherit"],
			})`git clone --progress ${repository} ${specificRepositoryLocation}`;

			console.log("Repository cloned");
			return "Repository cloned";
		} catch (error) {
			if(error instanceof Error) {
				console.log("Error while clone of repository : " + error.message);
				return "Error while clone of repository : " + error.message;
			} else {
				console.log("Error");
				return "Error";
			}
		}
}
