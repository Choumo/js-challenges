import type { Prompt } from "@poppinss/prompts";

import { execa } from "execa";

import { checkIfIsGitProject } from "../services/check_project.js";

export async function pushCommit(prompt: Prompt) {
	if (!await checkIfIsGitProject()) {
		throw new Error('You are not inside a Git repository');
	}

	const { stdout: branchStdout } = await execa('git', ['rev-parse', '--abbrev-ref', 'HEAD']);
	const currentBranch = branchStdout.trim();

	const remoteName: string = await prompt.ask('Enter the remote name (default = "origin"):', {
		default: 'origin'
	});

	const branchName: string = await prompt.ask(`Enter the branch name (default = "${currentBranch}"):`, {
		default: currentBranch
	});

	const forcePush: boolean = await prompt.confirm('Do you want to force push? (default = false)', {
		default: false
	});

	const dryRun: boolean = await prompt.confirm('Do you want to perform a dry run? (default = false)', {
		default: false
	});

	const pushArgs = ['push', remoteName, branchName];
	if (forcePush) {
		pushArgs.push('--force');
	}
	if (dryRun) {
		pushArgs.push('--dry-run');
	}

	try {
		await execa('git', pushArgs);
		console.log(`Pushed branch '${branchName}' to remote '${remoteName}' successfully.`);
		return `Pushed branch '${branchName}' to remote '${remoteName}' successfully.`;
	} catch (error) {
		if (error instanceof Error) {
			console.error('Failed to push :', error.message);
			return 'Failed to push the branch:' + error.message;
		} else {
			console.error('Failed to push the branch:', error);
			return '';
		}
	}
}
