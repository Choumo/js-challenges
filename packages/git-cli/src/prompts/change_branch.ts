import type { Prompt } from "@poppinss/prompts";

import { execa } from "execa";

import { checkIfIsGitProject } from "../services/check_project.js";

//TODO : Problème si commit changement qui sont vides
export async function ChangeBranch(prompt: Prompt) {
	if (!await checkIfIsGitProject()) {
		throw new Error('You are not inside a Git repository');
	}

	let { stdout } = await execa('git', ['branch']);
	const branchList = stdout.trim().split('\n').map(branch => branch.trim().replace('* ', ''));

	let targetBranch: string;

	if (branchList.length > 1) {
		const selectExisting: boolean = await prompt.confirm("Do you want to switch to an existing branch?");

		if (selectExisting) {
			targetBranch = await prompt.choice("Select the branch you want to switch to:", branchList);
		} else {
			targetBranch = await prompt.ask('What is the name of the new branch?', {
				validate(value: string): boolean {
					return value.length > 0;
				}
			});

			const bSelectCreateBranch: boolean = await prompt.confirm("Do you want to create the new branch from another one?");
			if (bSelectCreateBranch) {
				const baseBranch = await prompt.choice("Select the base branch:", branchList);
				await execa('git', ['branch', targetBranch, baseBranch]);
			} else {
				await execa('git', ['branch', targetBranch]);
			}
		}
	} else {
		targetBranch = await prompt.ask('What is the name of the new branch?', {
			validate(value: string): boolean {
				return value.length > 0;
			}
		});

		await execa('git', ['branch', targetBranch]);
	}

	const saveChanges: boolean = await prompt.confirm("Do you want to save current changes before switching branches?", { default: false });

	if (saveChanges) {
		const commitMessage: string = await prompt.ask('Enter a commit message for the current changes:', {
			validate(value: string): boolean {
				return value.length > 0;
			}
		});

		await execa('git', ['add', '.']);
		await execa('git', ['commit', '-m', commitMessage]);
		console.log('Changes saved successfully.');
	}

	// Switch to the selected or newly created branch
	await execa('git', ['checkout', targetBranch]);

	console.log(`Switched to branch '${targetBranch}' successfully.`);
	return `Switched to branch '${targetBranch}' successfully.`;
}
