import type { Prompt } from "@poppinss/prompts";

import { execa } from "execa";

import { checkIfIsGitProject } from "../services/check_project.js";

export async function pullChanges(prompt: Prompt) {
	if (!await checkIfIsGitProject()) {
		throw new Error('You are not inside a Git repository');
	}

	const { stdout: branchStdout } = await execa('git', ['rev-parse', '--abbrev-ref', 'HEAD']);
	const currentBranch = branchStdout.trim();

	const remoteName: string = await prompt.ask('Enter the remote name (default = "origin"):', {
		default: 'origin'
	});

	const branchName: string = await prompt.ask(`Enter the branch name (default = "${currentBranch}"):`, {
		default: currentBranch
	});

	const fetchArgs = ['fetch', remoteName, branchName];

	try {
		await execa('git', fetchArgs);
		console.log(`Fetched changes from '${remoteName}/${branchName}' successfully.`);

		return `Fetched changes from '${remoteName}/${branchName}' successfully.`;
	} catch (error) {
		if (error instanceof Error) {
			console.error('Failed to fetch changes:', error.message);
			return 'Failed to fetch changes';
		} else {
			console.error('Failed to fetch changes:', error);
			return 'Failed to fetch changes';
		}
	}
}
