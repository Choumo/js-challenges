import type {Prompt} from "@poppinss/prompts";

import {addFiles} from "./add_files_prompt.js";
import {ChangeBranch} from "./change_branch.js";
import {ClonePrompt} from "./clone_prompt.js";
import {commitChanges} from "./commit_prompt.js";
import {CreateBranch} from "./create_branch.js";
import {pullChanges} from "./pull_prompt.js";
import {pushCommit} from "./push_prompt.js";
import {AddRemote} from "./remote_prompt.js";

export async function defaultPrompt(prompt: Prompt){
	const choice = await prompt.choice("Select an action", [
		{
			name: "clone",
			message: "Clone a repository",
		},
		{
			name: "remote",
			message: "Add a remote origin"
		},
		{
			name: "create branch",
			message: "Create a new branch"
		},
		{
			name: "change branch",
			message: "Change your current branch"
		},
		{
			name: "add files",
			message: "Add files to your commit"
		},
		{
			name: "create commit",
			message: "Create a new commit"
		},
		{
			name: "push commit",
			message: "Push your commit"
		},
		{
			name: "pull changes",
			message: "Pull the changes"
		},
	]);

	switch (choice) {
		case "clone": {
			return ClonePrompt(prompt);
		}
		case "remote": {
			return AddRemote(prompt);
		}
		case "create branch": {
			return CreateBranch(prompt);
		}
		case "change branch": {
			return ChangeBranch(prompt);
		}
		case "add files": {
			return addFiles(prompt);
		}
		case "create commit": {
			return commitChanges(prompt);
		}
		case "push commit": {
			return pushCommit(prompt);
		}
		case "pull changes": {
			return pullChanges(prompt);
		}
	}
}
