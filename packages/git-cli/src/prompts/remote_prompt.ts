import type {Prompt} from "@poppinss/prompts";

import {execa} from "execa";

import {checkIfIsGitProject} from "../services/check_project.js";


export async function AddRemote(prompt: Prompt) {
	if(!await checkIfIsGitProject()) {
		throw new Error('Your are not inside a Git repository');
	}

	const remoteUrl: string = await prompt.ask('What is the remote URL?', {
		validate(value: string) {
			return value.length > 0 && value.startsWith('https://');
		}
	});

	const bAddRemoteName: boolean = await prompt.confirm("Do you want to add a remote name?");

	let remoteName = "origin";

	if(bAddRemoteName) {
		remoteName = await prompt.ask("Type the name of the remote (default : origin) : ", {
			validate(value: string): boolean {
				return value.length > 0;
			}
		});
	}

	await execa({
		stdout: ["pipe", "inherit"],
	})`git remote add ${remoteName} ${remoteUrl}`;

	console.log("Remote set");
	return 'Remote set';
}
