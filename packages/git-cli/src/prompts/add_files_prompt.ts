import type { Prompt } from "@poppinss/prompts";

import { execa } from "execa";

import { checkIfIsGitProject } from "../services/check_project.js";


//TODO : Erreur : Lancer node index.js depuis le repertoire js-challenges, sinon problème au niveau du path des fichiers
export async function addFiles(prompt: Prompt) {
	if (!await checkIfIsGitProject()) {
		throw new Error('You are not inside a Git repository');
	}

	const { stdout } = await execa('git', ['status', '--porcelain']);
	const modifiedFiles = stdout.trim().split('\n').filter(Boolean).map(line => line.slice(2));

	if (modifiedFiles.length === 0) {
		console.log('No modified files to commit.');
		return;
	}

	console.log('Modified files:');
	for (const file of modifiedFiles) console.log(`- ${file}`);

	const selectedFiles: Array<string> = await prompt.multiple('Select files to add to the commit:', modifiedFiles, {
		validate(value: Array<string>): boolean {
			return value.length > 0;
		}
	});

	try {
		await execa('git', ['add', ...selectedFiles]);

		console.log('Files added to the commit:');
		for (const file of selectedFiles) console.log(`- ${file}`);

		return 'Files added to the commit:';
	} catch (error) {
		if (error instanceof Error) {
			console.error('Failed to add files to the commit:', error.message);

			return 'Failed to add files to the commit';
		} else {
			console.error('Failed to add files to the commit:', error);
			return 'Failed to add files to the commit';
		}
	}
}
