import type { Prompt } from "@poppinss/prompts";

import { execa } from "execa";

import { checkIfIsGitProject } from "../services/check_project.js";

export async function commitChanges(prompt: Prompt) {
	if (!await checkIfIsGitProject()) {
		throw new Error('You are not inside a Git repository');
	}

	const { stdout: diffStdout } = await execa('git', ['diff', '--cached', '--name-only']);
	const stagedFiles = diffStdout.trim().split('\n').filter(Boolean);

	if (stagedFiles.length === 0) {
		console.log('No changes staged for commit.');
		return;
	}

	console.log('Staged files:');
	for (const file of stagedFiles) console.log(`- ${file}`);


	const commitMessage: string = await prompt.ask('Enter the commit message:', {
		validate(value: string): boolean {
			return value.length > 0;
		}
	});

	const commitDescription: string = await prompt.ask('Enter the commit description (optional):');

	const fullCommitMessage = commitDescription ? `${commitMessage}\n\n${commitDescription}` : commitMessage;

	try {
		await execa('git', ['commit', '-m', fullCommitMessage]);
		console.log('Changes committed successfully.');

		return 'Changes committed successfully.';
	} catch (error) {
		if (error instanceof Error) {
			console.error('Failed :', error.message);
			return 'Failed to commit changes:' + error.message;
		} else {
			console.error('Failed to commit changes:', error);
			return 'Error';
		}
	}
}
