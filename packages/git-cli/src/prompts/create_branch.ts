import type {Prompt} from "@poppinss/prompts";

import {execa} from "execa";

import {checkIfIsGitProject} from "../services/check_project.js";

export async function CreateBranch(prompt: Prompt) {
	if(!await checkIfIsGitProject()) {
		throw new Error('Your are not inside a Git repository');
	}

	const newBranch: string = await prompt.ask('What is the name of the new branch?', {
		validate(value: string): boolean {
			return value.length > 0;
		}
	});

	let { stdout } = await execa('git', ['branch']);
	const branchList = stdout.trim().split('\n');

	let currentBranch = '';

	if(branchList.length > 0) {
		for (let i = 0; i < branchList.length; i++) {
			if (branchList[i]!.startsWith('*')) {
				currentBranch = branchList[i]!.slice(2).trim();
			}
			branchList[i] = branchList[i]!.slice(2).trim();
		}
	}

	const bSelectCreateBranch: boolean = await prompt.confirm("Do you want to create the new branch from another one?");
	if(bSelectCreateBranch) {
		currentBranch = await prompt.choice("Select the base branch?", branchList);
	}


	try {
		await execa({
			stdout: ["pipe", "inherit"],
		})`git branch ${newBranch} ${currentBranch}`;

		console.log("Branch created successfully.");

		return 'Branch created successfully.';
	} catch (error) {
		if (error instanceof Error) {
			console.error('Failed :', error.message);
			return 'Failed to create branch:' + error.message;
		} else {
			console.error('Failed to create branch:', error);
			return 'Error';
		}
	}
}
