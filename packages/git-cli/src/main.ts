import type { Prompt } from '@poppinss/prompts';

import { defaultPrompt } from "./prompts/menu_prompt.js";

export class GitCli {
	#prompt: Prompt;

	constructor(prompt: Prompt) {
		this.#prompt = prompt;
	}

	async run() {
		try {
			return await defaultPrompt(this.#prompt);
		} catch (error: unknown) {
			console.error(error);
			return '';
		}
	}
}
