import assert from 'node:assert';
import test from 'node:test';

import { Prompt } from '@poppinss/prompts';

import { GitCli } from '../src/main.js';

void test('Change current branch', async () => {
	const prompt = new Prompt();
	const gitCli = new GitCli(prompt);

	prompt.trap('Select an action').chooseOption(3);
	prompt.trap('Do you want to switch to an existing branch?').reject();
	prompt.trap('What is the name of the new branch?').replyWith("testbranch");
	prompt.trap('Do you want to create the new branch from another one?').reject();
	prompt.trap('Do you want to save current changes before switching branches?').reject();

	const result = await gitCli.run();
	assert.strictEqual(result, 'Switched to branch \'testbranch\' successfully.');
});
