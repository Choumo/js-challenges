import assert from 'node:assert';
import test from 'node:test';

import { Prompt } from '@poppinss/prompts';

import { GitCli } from '../src/main.js';

void test('push commit', async () => {
	const prompt = new Prompt();
	const gitCli = new GitCli(prompt);

	prompt.trap('Select an action').chooseOption(6);
	prompt.trap('Enter the remote name (default = "origin"):').replyWith("origin");
	prompt.trap('Enter the branch name (default = "main"):').replyWith("main");
	prompt.trap('Do you want to force push? (default = false)').reject();
	prompt.trap('Do you want to perform a dry run? (default = false)').reject();

	const result = await gitCli.run();
	assert.strictEqual(result, 'Pushed branch \'main\' to remote \'origin\' successfully.');
});
