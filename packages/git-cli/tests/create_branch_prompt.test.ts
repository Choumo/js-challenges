import assert from 'node:assert';
import test from 'node:test';

import { Prompt } from '@poppinss/prompts';

import { GitCli } from '../src/main.js';

void test('create branch', async () => {
	const prompt = new Prompt();
	const gitCli = new GitCli(prompt);

	prompt.trap('Select an action').chooseOption(2);
	prompt.trap('What is the name of the new branch?').replyWith("test-branch");
	prompt.trap('Do you want to create the new branch from another one?').reject();

	const result = await gitCli.run();
	assert.strictEqual(result, 'Branch created successfully.');
});
