import assert from 'node:assert';
import test from 'node:test';

import { Prompt } from '@poppinss/prompts';

import { GitCli } from '../src/main.js';

void test('Commit changes', async () => {
	const prompt = new Prompt();
	const gitCli = new GitCli(prompt);

	prompt.trap('Select an action').chooseOption(5);
	prompt.trap('Enter the commit message:').replyWith("This is a test message");
	prompt.trap('Enter the commit description (optional):').replyWith("This is a test description");

	const result = await gitCli.run();
	assert.strictEqual(result, 'Changes committed successfully.');
});
