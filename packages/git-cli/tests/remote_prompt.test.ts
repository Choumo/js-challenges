import assert from 'node:assert';
import test from 'node:test';

import { Prompt } from '@poppinss/prompts';

import { GitCli } from '../src/main.js';

void test('Add remote', async () => {
	const prompt = new Prompt();
	const gitCli = new GitCli(prompt);

	const gitUrl = "some-url";

	prompt.trap('Select an action').chooseOption(1);
	prompt.trap('What is the remote URL?').replyWith(gitUrl);
	prompt.trap('Do you want to add a remote name?').reject();

	const result = await gitCli.run();
	assert.strictEqual(result, 'Remote set');
});
