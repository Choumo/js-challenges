import assert from 'node:assert';
import test from 'node:test';

import { Prompt } from '@poppinss/prompts';

import { GitCli } from '../src/main.js';

void test('pull changes', async () => {
	const prompt = new Prompt();
	const gitCli = new GitCli(prompt);

	prompt.trap('Select an action').chooseOption(7);
	prompt.trap('Enter the remote name (default = "origin"):').replyWith("origin");
	prompt.trap('Enter the branch name (default = "main"):').replyWith("main");

	const result = await gitCli.run();
	assert.strictEqual(result, 'Fetched changes from \'origin/main\' successfully.');
});
