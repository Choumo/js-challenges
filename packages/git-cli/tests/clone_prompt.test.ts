import assert from 'node:assert';
import test from 'node:test';

import { Prompt } from '@poppinss/prompts';

import { GitCli } from '../src/main.js';

void test('clone repo on specific location', async () => {
	const prompt = new Prompt();
	const gitCli = new GitCli(prompt);

	const gitPath = "/custom/valid/path";

	prompt.trap('Select an action').chooseOption(0);
	prompt.trap('Select a type').chooseOption(0);
	prompt.trap('Paste your repository url : ').replyWith("https://gitlab.com/Choumo/js-challenges.git");
	prompt.trap('Do you want to clone the repository at a specific location ?').accept();
	prompt.trap('Select the absolute path or the name of where you want to clone the repository (default : current directory): ').replyWith(gitPath);

	const result = await gitCli.run();
	assert.strictEqual(result, 'Repository clone');
});
