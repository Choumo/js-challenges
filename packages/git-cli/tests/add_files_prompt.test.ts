import assert from 'node:assert';
import test from 'node:test';

import { Prompt } from '@poppinss/prompts';

import { GitCli } from '../src/main.js';

void test('Change current branch', async () => {
	const prompt = new Prompt();
	const gitCli = new GitCli(prompt);

	prompt.trap('Select an action').chooseOption(4);
	prompt.trap('Select files to add to the commit:').chooseOption(0);

	const result = await gitCli.run();
	assert.notStrictEqual(result, 'Files added to the commit:');
});
